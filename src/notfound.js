import React from 'react';

const NotFound = () => <h1>ERROR 404: Page not found</h1>

export default NotFound;